// clang-format off
// stupid includes depend on order!
#include <tonc.h>
#include <posprintf.h>
#include <comm.h>
#include <gbfs.h>
// clang-format on

extern const GBFS_FILE assets_gbfs;

void Boot(const u8 *program, u32 size) {
	// disable interrupts
	REG_IME = 0;
	// copy program to ewram
	dma3_cpy((void *)MEM_EWRAM, program, size);
	// reset everything *but* ewram
	RegisterRamReset(RESET_IWRAM | RESET_PALETTE | RESET_VRAM | RESET_OAM | RESET_REG_SIO | RESET_REG_SOUND | RESET_REG);
	// set magic flag for reset to ewram
	REG_RESET_DST = RAM_RESTART;
	// execute reset
	SoftReset();
}

int main(void) {
	REG_DISPCNT = DCNT_MODE0 | DCNT_BG0;

	irq_init(NULL);
	irq_enable(II_VBLANK);

	tte_init_chr4c_default(0, BG_CBB(0) | BG_SBB(31));
	tte_set_pos(0, 0);

	u32 payloadLen;
	const void *payload = gbfs_get_obj(&assets_gbfs, "payload.gba", &payloadLen);
	if (!payload) {
		tte_write("Could not load payload.gba\n");
		while (1) { VBlankIntrWait(); }
	}

	while (1) {
		tte_write("Press A to send payload.\nPress B to boot locally.\n");
		while (1) {
			key_poll();
			if (key_hit(KEY_A)) { break; }
			if (key_hit(KEY_B)) { Boot(payload, payloadLen); }
			VBlankIntrWait();
		}
		tte_erase_screen();
		tte_set_pos(0, 0);
		int multibootStatus = __comm_multiboot_send(MB_CLIENT_ALL, payload, payloadLen, MB_PAL_ANIM(1, 1, 1));
		if (multibootStatus) {
			char buffer[41];
			posprintf(buffer, "Failed to send payload (%04x)\n", multibootStatus);
			tte_write(buffer);
			continue;
		}

		tte_write("Payload sent!\n");
	}

	return 0;
}

/*
Xilefian: Investigated the MultiBoot "palette" parameter for a bit.
Colour (0..7) decides the colours to be cycled for the Nintendo logo, the colours are:

0. Magenta -> Blue
1. Blue -> Cyan
2. Cyan -> Green
3. Green -> Yellow
4. Yellow -> Red
5. Red -> Magenta
6. Magenta -> White
7. Magenta (no animation)


Xilefian: direction flips the start/end colours


Xilefian: speed is how long it holds on one of the two colours before cycling
(it's all very vague and no-one has documented it)
*/
