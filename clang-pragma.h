#pragma once

#pragma clang diagnostic error "-Wall"
#pragma clang diagnostic error "-Wextra"
#pragma clang diagnostic error "-Wpedantic"
#pragma clang diagnostic error "-Wc++20-compat"
#pragma clang diagnostic error "-Wc++20-compat-pedantic"

#pragma clang diagnostic ignored "-Wc++98-compat"
#pragma clang diagnostic ignored "-Wc++11-compat"
#pragma clang diagnostic ignored "-Wc++14-compat"
#pragma clang diagnostic ignored "-Wc++17-compat"
