#pragma once
#include <string>
#include <string_view>

namespace {
	std::string concat(auto... args) {
		std::string result;
		const std::string_view views[]{args...};
		std::string::size_type full_size = 0;
		for (auto sub_view : views) full_size += sub_view.size();
		result.reserve(full_size);
		for (auto sub_view : views) result.append(sub_view);
		return result;
	}
}
