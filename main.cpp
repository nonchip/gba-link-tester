#include "clang-pragma.h"
#include "gba_helpers.hpp"
#include "modepage.hpp"

#include <gba/ext/agbabi.hpp>
#include <gba/ext/mgba.hpp>
#include <gba/gba.hpp>
#include <string>
#include <tonc.h>
#include <tonc_types.h>
#include <variant>

using namespace gba;
using namespace std;
using namespace modepages;

int main() {
	mgba::open();

	bios::register_ram_reset(bios::reset_flags{.palette = true, .vram = true, .oam = true});

	auto disp_mode = io::mode<4>::display_control().set_layer_background_2(true);
	reg::dispcnt::write(disp_mode);
	tte_init_bmp(4, &sys8Font, nullptr);

	// default palette:
	// pal_bg_mem[0xF1]= CLR_YELLOW;
	// pal_bg_mem[0xF2]= CLR_ORANGE;
	// pal_bg_mem[0]= CLR_BLACK;
	gba::rawmem::palette_memory[0xF3] = uint_cast(bgr555_t{31, 0, 0}); // red

	reg::dispstat::write(display_status{.vblank_irq = true});
	reg::ie::write(interrupt_mask{.vblank = true});
	reg::ime::emplace(true);

	ModePage<monostate>::variant mp;
	ModePage<monostate>::init(mp);

	TTC *tc = tte_get_context();
	while (true) {
		bios::vblank_intr_wait();
		tc->dst.data = m4_surface.data = const_cast<u8 *>(io::bitmap_page(!disp_mode.flip_page().get_page()));
		reg::dispcnt::write(disp_mode);

		try {
			ModePage<monostate>::update(mp);
		} catch (const exception &e) { mgba::printf(mgba::log_level::fatal, "Exception: %s", e.what()); }
	}
	mgba::printf(mgba::log_level::fatal, "Reached the unreachable!");
	__builtin_unreachable();
}
