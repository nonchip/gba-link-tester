#pragma once
#include <cstdint>
#include <type_traits>

template <typename T, std::uintptr_t A> struct constptr{
	using type = T;
	using value_type = std::remove_cvref_t<type>;
	constexpr static std::uintptr_t address = A;

	constexpr type* const operator& (               ) const { return reinterpret_cast<type*>(address); }
	constexpr type&       operator* (               ) const { return *operator&();                     }
	constexpr type&       operator[](std::size_t idx) const { return  operator&()[idx];                }
};

template<typename T, std::uintptr_t A> using volptr = constptr<volatile T,A>;
