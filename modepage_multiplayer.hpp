#pragma once

#include "clang-pragma.h"
#include "format.hpp"
#include "hexinput.hpp"
#include "str_cat.hpp"

#include <cstdint>
#include <gba/gba.hpp>
#include <string>
#include <tonc.h>
#include <variant>

namespace modepages {
	using namespace std;
	using namespace gba;
	using namespace gba::sio;
	template <class Mode> struct ModePage;
	template <> struct ModePage<monostate>;

	template <auto B = sio::multiplayer::baud_rate::bps_9600> struct multiplayer {};
	template <auto B> struct ModePage<multiplayer<B>> {
			string s_pid, s_rxw[4], s_baudrate;
			HexInput<uint16_t> input;

			void init() {
				r_control<sio::multiplayer> rcnt = {0, 0, 0, 0};
				reg::rcnt<sio::multiplayer>::write(rcnt);

				control<sio::multiplayer> cnt;
				cnt.baud_rate = B;
				reg::siocnt<sio::multiplayer>::write(cnt);
				s_baudrate = format("%s", _baudrates[static_cast<size_t>(cnt.baud_rate)]);
				input.set(0);
			}

			string update() {
				input.handle_keys();

				auto cnt = reg::siocnt<sio::multiplayer>::read();
				if (!cnt.transferring) {
					s_pid    = format("%d", static_cast<int>(cnt.player_id));
					s_rxw[0] = format("%#04X", reg::siomulti0::read());
					s_rxw[1] = format("%#04X", reg::siomulti1::read());
					s_rxw[2] = format("%#04X", reg::siomulti2::read());
					s_rxw[3] = format("%#04X", reg::siomulti3::read());

					if (cnt.input_terminal == sio::multiplayer::input_terminal::parent && key_hit(KEY_A)) {
						reg::siomlt_send::write(input.get());
						cnt.transferring = true;
						reg::siocnt<sio::multiplayer>::write(cnt);
					} else if (cnt.input_terminal == sio::multiplayer::input_terminal::child) {
						reg::siomlt_send::write(input.get());
					}
				}

				const auto is_tx_s     = (cnt.transferring ? "TX " : "   ");
				const auto is_error_s  = (cnt.error ? "E " : "  ");
				const auto is_master_s = (cnt.input_terminal == sio::multiplayer::input_terminal::parent ? "M " : "S ");
				const auto is_ready_s  = (cnt.data_terminal == sio::multiplayer::data_terminal::all_ready ? "R " : "N ");

				return concat( // clang-format off
				"MP ", is_tx_s, is_error_s, is_master_s, is_ready_s, s_pid,
				"\nBR: ", s_baudrate,
				"\nTX: ", input.render(),
				"\nR0: ", s_rxw[0],
				"\nR1: ", s_rxw[1],
				"\nR2: ", s_rxw[2],
				"\nR3: ", s_rxw[3],
				"#{P: 0,120} A :transmit\n + :change data"
			); // clang-format on
			}

		private:
			static constexpr const char *_baudrates[] = {"9600", "38400", "57600", "115200"};
	};
}
