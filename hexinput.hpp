#pragma once
#include "format.hpp"

#include <string>
#include <sys/_stdint.h>
#include <tonc.h>
#include <tonc_input.h>

namespace {
	template <typename T> struct HexInput {
			enum scrollHit { none, left, right };

			bool changed() {
				bool ret = _changed;
				_changed = false;
				return ret;
			}

			T get() { return _value; }
			void set(T v) { _value = v; }

			std::string render(bool focus = true) {
				std::string out;
				for (uint8_t i = 0; i < _digits; i++) {
					auto *fmt = &_blur;
					if (focus) {
						auto which = _digits - _which - 1;
						if (i == which)
							fmt = &_highlighted;
						else
							fmt = &_focus;
					}
					out += format(*fmt, getNibble(_digits - i - 1));
				}
				return out;
			}

			constexpr scrollHit handle_keys() {
				if (key_hit(KEY_RIGHT)) return scrollWhich(-1);
				if (key_hit(KEY_LEFT)) return scrollWhich(+1);

				if (key_hit(KEY_UP))
					addNibble(+1, _which);
				else if (key_hit(KEY_DOWN))
					addNibble(-1, _which);
				return none;
			}

		protected:
			constexpr uint8_t getNibble(uint8_t which) { return (_value >> (which * 4)) & 0xF; }

			void setNibble(uint8_t nibble, uint8_t which) {
				if (nibble == getNibble(which)) return;
				_value   = (_value & ~(0xF << (which * 4))) | ((nibble & 0xF) << (which * 4));
				_changed = true;
			}

			constexpr void addNibble(int8_t delta, uint8_t which) { setNibble((getNibble(which) + delta) % 0x10, which); }
			constexpr scrollHit scrollWhich(int8_t delta) {
				int16_t nw = static_cast<int16_t>(_which) + delta;
				if (nw >= _digits) {
					_which = _digits - 1;
					return left;
				}
				if (nw < 0) {
					_which = 0;
					return right;
				}
				_which = nw;
				return none;
			}

		private:
			constexpr static const char *_blur        = "%01X";
			constexpr static const char *_focus       = "#{ci:0xF2}%01X#{ci:0xF1}";
			constexpr static const char *_highlighted = "#{ci:0xF3}%01X#{ci:0xF1}";

			constexpr static const uint8_t _digits = sizeof(T) * 2;
			T _value;

			bool _changed  = false;
			uint8_t _which = 0;
	};
}
