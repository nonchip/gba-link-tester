#pragma once
#include <variant>

namespace {
	template <typename... Ts, std::size_t... Is> constexpr void next(std::variant<Ts...> &v, std::index_sequence<Is...>) {
		using Func         = void (*)(std::variant<Ts...> &);
		const Func funcs[] = {+[](std::variant<Ts...> &v) { v.template emplace<(Is + 1) % sizeof...(Is)>(); }...};
		funcs[v.index()](v);
	}

	template <typename... Ts> constexpr void next(std::variant<Ts...> &v) {
		next(v, std::make_index_sequence<sizeof...(Ts)>());
	}

	template <typename... Ts, std::size_t... Is> constexpr void prev(std::variant<Ts...> &v, std::index_sequence<Is...>) {
		using Func = void (*)(std::variant<Ts...> &);
		const Func funcs[]
		 = {+[](std::variant<Ts...> &v) { v.template emplace<(Is + sizeof...(Is) - 1) % sizeof...(Is)>(); }...};
		funcs[v.index()](v);
	}

	template <typename... Ts> constexpr void prev(std::variant<Ts...> &v) {
		prev(v, std::make_index_sequence<sizeof...(Ts)>());
	}
}
