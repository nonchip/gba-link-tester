#pragma once
#include "constptr.hpp"

#include <gba/gba.hpp>

namespace gba {

	namespace rawmem {

		constexpr volptr<uint16, 0x05000000> palette_memory;
		constexpr volptr<uint8, 0x06000000> bitmap_memory;
		constexpr volptr<uint8, 0x07000000> object_memory;

	}

	struct bgr555_t {
			uint16 red : 5;
			uint16 green : 5;
			uint16 blue : 5;
	};

	namespace io {

		constexpr volatile uint8 *bitmap_page(bool page) noexcept { return &rawmem::bitmap_memory[page * 0xA000]; }
	}
}
