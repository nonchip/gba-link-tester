#pragma once

#include "clang-pragma.h"
#include "format.hpp"
#include "gba/registers/sio.hpp"
#include "gba/sio/normal.hpp"
#include "hexinput.hpp"
#include "str_cat.hpp"

#include <cstdint>
#include <gba/gba.hpp>
#include <string>
#include <tonc.h>
#include <type_traits>
#include <variant>

namespace modepages {
	using namespace std;
	using namespace gba;
	using namespace gba::sio;
	template <class Mode> struct ModePage;
	template <> struct ModePage<monostate>;

	template <auto S = sio::normal::shift_clock::external, auto L = sio::normal::transfer_type::byte> struct normal {};
	template <auto S, auto L> struct ModePage<normal<S, L>> {
			using data_type = typename std::conditional<L == sio::normal::transfer_type::byte, uint8_t, uint32_t>::type;
			using reg_data =
			 typename std::conditional<L == sio::normal::transfer_type::byte, reg::siodata8, reg::siodata32>::type;

			string s_clk, is_ready_s;
			HexInput<data_type> input;

			void init() {
				r_control<sio::normal> rcnt = {0, 0, 0, 0};
				reg::rcnt<sio::normal>::write(rcnt);
				control<sio::normal> cnt;
				cnt.shift_clock  = S;
				cnt.type         = L;
				cnt.output_state = sio::normal::io_state::ready;
				reg::siocnt<sio::normal>::write(cnt);
				s_clk = format("%s", _clocks[static_cast<size_t>(S)]);
			}

			string update() {
				input.handle_keys();

				auto cnt            = reg::siocnt<sio::normal>::read();
				const auto is_ready = cnt.input_state == sio::normal::io_state::ready;

				if (!cnt.transferring) {
					if (input.changed())
						reg_data::write(input.get());
					else
						input.set(reg_data::read());

					if (key_hit(KEY_A) && (S == sio::normal::shift_clock::external || is_ready)) {
						cnt.transferring = true;
						reg::siocnt<sio::normal>::write(cnt);
					}
				}
				const auto is_tx_s    = (cnt.transferring ? "TX " : "   ");
				const auto is_ready_s = (is_ready ? "R " : " ");
				return concat( // clang-format off
				(L==sio::normal::transfer_type::byte?"NB ":"NW "), is_tx_s, is_ready_s, " ", s_clk,
				"\nDATA: ", input.render(),
				"#{P: 0,120} A :transmit\n + :change data"
			); // clang-format on
			}

		private:
			static constexpr const char *_clocks[] = {"ExtClk", "256Khz", "N/A", "2MHz"};
	};
}
