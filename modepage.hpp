#pragma once
#include "clang-pragma.h"
#include "modepage_multiplayer.hpp"
#include "modepage_normal.hpp"
#include "variant_incdec.hpp"

#include <gba/gba.hpp>
#include <string>
#include <tonc.h>
#include <variant>

namespace modepages {
	using namespace std;
	using namespace gba;

	template <class Mode> struct ModePage {};

	template <> struct ModePage<monostate> {
			using baud_rate       = sio::multiplayer::baud_rate;
			using shift_clock     = sio::normal::shift_clock;
			using transfer_length = sio::normal::transfer_type;
			using variant         = std::variant< // clang-format off
				ModePage<multiplayer<baud_rate::bps_9600>>,
				ModePage<multiplayer<baud_rate::bps_38400>>,
				ModePage<multiplayer<baud_rate::bps_57600>>,
				ModePage<multiplayer<baud_rate::bps_115200>>,
				ModePage<normal<shift_clock::external,        transfer_length::byte>>,
				ModePage<normal<shift_clock::internal_256Khz, transfer_length::byte>>,
				ModePage<normal<shift_clock::internal_2MHz,   transfer_length::byte>>,
				ModePage<normal<shift_clock::external,        transfer_length::word>>,
				ModePage<normal<shift_clock::internal_256Khz, transfer_length::word>>,
				ModePage<normal<shift_clock::internal_2MHz,   transfer_length::word>>
			>; // clang-format on

			static void init(variant &mp) {
				bios::register_ram_reset(bios::reset_flags{.reg_sio = true});
				visit(_init, mp);
			}

			static void handle_keys(variant &mp) {
				key_poll();
				if (key_hit(KEY_B)) return init(mp);
				if (key_hit(KEY_R)) {
					next(mp);
					return init(mp);
				}
				if (key_hit(KEY_L)) {
					prev(mp);
					return init(mp);
				}
			}

			static void update(variant &mp) {
				handle_keys(mp);
				string ui_str = "#{es; P}        GBA LINK TESTER\n\n"s;
				ui_str += visit(_update, mp);
				ui_str += "#{P: 0,140} B :reset\nL/R:change mode";
				tte_write(ui_str.c_str());
			}

		private:
			constexpr static auto _init   = [](auto &page) { return page.init(); };
			constexpr static auto _update = [](auto &page) { return page.update(); };
	};
}
